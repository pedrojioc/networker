/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import {Calls} from '../config/router'

export default class ContactBox extends Component {

  //(data) datos del contacto a llamar
  onCall = (data) => {
    this.props.nav.navigate('UserCall', { data: data })
  }
  onVideoCall(data){
    this.props.nav.navigate('OutboundVideoCall', { data: data })
  }

  showModalCall = (data) => {
    Alert.alert(
      'Alert Title',
      'My Alert Msg',
      [
        {text: 'Cancel', onPress: () => console.log('Ask me later pressed')},
        {text: 'VIDEOCALL', onPress: () => this.onVideoCall(data)},
        {text: 'CALL', onPress: () => this.onCall(data)},
      ],
      { cancelable: false }
    )
  }
  render() {
    const { user_id, one_signal, username } = this.props.data
    return (
      <View style={styles.contactBox}>
        <View style={styles.containerImageName}>
          <Image style={styles.image} source={require('../assets/img/avatar.png')} />
          <Text style={styles.name}>{username}</Text>
        </View>
        <View style={styles.containerImgIcon}>
          <TouchableOpacity  onPress={() => this.showModalCall(this.props.data)}>
            <MaterialIcons
            name = 'more-vert'
            size = {24}
            style = {{color: '#95989A'}}
            >
            </MaterialIcons>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contactBox:{
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  image:{
    width: 60,
    height: 60
  },
  containerImageName:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  name:{
    color: '#95989A',
    fontSize: 20,
    fontWeight: '400',
    marginLeft: 10
  },
  containerImgIcon:{
    flexDirection: 'row',
  },
  imageCall:{
    width: 30,
    height: 30
  }
});

//AppRegistry.registerComponent('Networker', () => Networker);
