/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  StatusBar,
  TouchableOpacity
} from 'react-native';

import { Header } from 'react-native-elements'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { StackNavigator, DrawerNavigator } from 'react-navigation'

export default class HeaderNav extends Component {

  renderButton(){
    return(
      <TouchableOpacity style={styles.btnOpenMenu} onPress={() => this.props.nav.navigate('DrawerOpen')}>
        <MaterialIcons
          name='menu'
          size = {30}
          color='#fff'
        >
        </MaterialIcons>
      </TouchableOpacity>
    );
  }

  render() {
    const title = this.props.title
    const search = this.props.search? { icon: 'search', color: '#fff' } : null
    return (
      <View style={styles.headerContent}>
        <StatusBar
         backgroundColor="#4169ff"
         barStyle="light-content"
         translucent={false}
       />
        <ImageBackground style={styles.imageback} source={require('../assets/img/header_3.png')}>
          <Header
            leftComponent={this.renderButton()}
            centerComponent={{ text: title, style: { color: '#fff', fontSize: 21, fontWeight: '400' } }}
            rightComponent={search}
            style={styles.header}
          />
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageback:{
    flex:1,
    width:null,
    height:50,
    justifyContent: 'center'
  },
  headerContent:{
    flexDirection: 'row',
    width: null,
    height:50,
    justifyContent: 'center'
  },
  header:{
    width: null,
    height:30
  },
  btnOpenMenu:{
    paddingLeft: 5,
    paddingRight: 5
  }
});
