/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet, 
  View,
  Button,
  ImageBackground,
  Image,
  FlatList
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import HistoryBox from '../core/HistoryBox'

export default class HistoryCall extends Component {
  /*========================Constructor========================*/
  constructor(props) {
    super(props);
    this.state = {
      dataSource: []
    }
  }

  componentDidMount() {
    this.setState({
      dataSource: this.props.records
    })
  }

  componentWillReceiveProps(newProps) {
    if(newProps.records !== this.props.records){
      this.updateDataSource(newProps.records)
    }
  }

  updateDataSource = data => {
    this.setState({
      dataSource: data
    })
  }

  render() {
    const image = '../assets/img/avatar.png'
    return (
      <FlatList
        data={this.state.dataSource}
        renderItem={({item}) => (
          <HistoryBox record={item}/>
        )}
        keyExtractor={item => item.history_id}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  content:{
    padding: 10
  },
});

