/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet, 
  View,
  Image
} from 'react-native';

export default class CallBox extends Component {
  render() {
    const { history_id, from, to, date, time, duration, call_state } = this.props.record
    const image = '../assets/img/avatar.png'
    const icons_call = {
      1: require('../assets/img/icons/outgoing_call.png'),
      2: require('../assets/img/icons/incoming_call_successful.png'),
      3: require('../assets/img/icons/outgoing_call.png'),
      4: require('../assets/img/icons/incoming_call_failed.png'),
    }
    
    return (
      <View style={styles.callBox}>
        <View style={styles.containerImageName}> 
          <Image style={styles.image} source={require('../assets/img/avatar.png')} />
          <Text style={styles.name}>{from}</Text>
        </View>
        <View style={styles.containerImgCall}>
          <Image style={styles.imageCall} source={icons_call[call_state]} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
  callBox:{
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  image:{
    width: 60,
    height: 60
  },
  containerImageName:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  name:{
    color: '#95989A',
    fontSize: 20,
    fontWeight: '400',
    marginLeft: 10
  },
  containerImgCall:{
    flexDirection: 'row',
  },
  imageCall:{
    width: 30,
    height: 30
  }
});

