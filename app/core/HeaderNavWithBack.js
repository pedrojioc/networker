/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  StatusBar
} from 'react-native';

import { Header } from 'react-native-elements'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { StackNavigator, DrawerNavigator } from 'react-navigation'

export default class HeaderNavWithBack extends Component {

  renderButton(){
    return(
      <MaterialIcons
        name='navigate-before'
        size = {35}
        color='#fff'
        onPress={() => this.props.nav.navigate(this.props.back)}
      >
      </MaterialIcons>
    );
  }

  render() {
    const title = this.props.title
    const search = this.props.search? { icon: 'search', color: '#fff' } : null
    return (
      <View style={styles.headerContent}>
        <StatusBar
         backgroundColor="#4169ff"
         barStyle="light-content"
       />
        <View style={styles.wrapperHeader}>
          <Header
            leftComponent={this.renderButton()}
            centerComponent={{ text: title, style: { color: '#fff', fontSize: 20, fontWeight: '400', fontFamily:"Roboto-Medium", marginBottom:5} }}
            rightComponent={search}
            style= {styles.header}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapperHeader:{
    flex:1,
    width:null,
    height:50,
    justifyContent: 'center'
  },
  headerContent:{
    flexDirection: 'row',
    width: null,
    height:50,
    justifyContent: 'center',
    backgroundColor: 'gray'
  },
  header:{
    width: null,
    height:50,
    backgroundColor: '#4169FF',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowRadius: 5,
    shadowOpacity: 1.0
  }
});
