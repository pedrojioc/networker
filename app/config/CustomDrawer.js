import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { DrawerItems, StackNavigator, DrawerNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import { onSignOut } from "../auth";
export default class SideDrawer extends PureComponent {

  logOut(){
    onSignOut()
      .then(() => {
        this.props.nav.navigation.navigate('SignedOut');
      })
  }
	render(){
		return (
			<View>
				<View style={styles.header}>
					<ImageBackground style={styles.imagemenu} source={require('../assets/img/header_1.png')}>
						<View style={styles.contentAvatar}>
							<Image style={styles.avatar} source={require('../assets/img/small_avatar.png')}/>
							<Text style={styles.name}>Pedro José</Text>
						</View>
        	</ImageBackground>
				</View>
				<DrawerItems {...this.props.nav}
          labelStyle={{color:'#95989A'}}
				/>
        <View style={styles.logout}>
          <TouchableOpacity style={styles.logOut} onPress={() => this.logOut()}>
            <Icon
              name = 'ios-log-out-outline'
              size = {24}
              style = {{color: '#757575', marginRight: 33}}
            >
            </Icon>
            <Text style={styles.textLogout}>Logout</Text>
          </TouchableOpacity>
        </View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  header:{

  },
  imagemenu: {
    width: 300,
    height: 150
  },
  contentAvatar:{
  	width: 80,
  	marginTop: 20,
  	marginLeft: 15,
  },
  avatar:{
  	width: 80,
  },
  name:{
  	marginTop: 15,
  	fontWeight: '600',
  	color: '#ffffff',
  	textAlign: 'center'
  },
  logOut:{
    flexDirection: 'row',
    paddingTop: 15,
    paddingLeft: 20,
    paddingBottom: 15
  },
  textLogout:{
    fontSize: 15,
    fontWeight: '500',
    color: '#95989A'
  }
});
