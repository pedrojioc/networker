import React from 'react';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import Icon from 'react-native-vector-icons/Ionicons';

import Login from '../screens/Login'
import Dashboard from '../screens/Dashboard'
import Contact from '../screens/Contact'
import HistoryCall from '../screens/HistoryCall'
import PathOfSuccess from '../screens/PathOfSuccess/PathOfSuccess'
import PathOfSuccess1 from '../screens/PathOfSuccess/PathOfSuccess1'
import PathOfSuccess3 from '../screens/PathOfSuccess/PathOfSuccess3'
import PathOfSuccess5 from '../screens/PathOfSuccess/PathOfSuccess5'
import PathOfSuccess7 from '../screens/PathOfSuccess/PathOfSuccess7'
import PathOfSuccess8 from '../screens/PathOfSuccess/PathOfSuccess8'
import PathOfSuccess9 from '../screens/PathOfSuccess/PathOfSuccess9'
import PathOfSuccess10 from '../screens/PathOfSuccess/PathOfSuccess10'
import PathOfSuccess11 from '../screens/PathOfSuccess/PathOfSuccess11'
import CallInProgress from '../screens/CallInProgress'
import OutboundVideoCall from '../screens/OutboundVideoCall'
import SideDrawer from './CustomDrawer'

export const SignedOut = StackNavigator(
  {
    SignIn: {
      screen: Login,
    }
  },
  {
    headerMode: 'none'
  }
);


export const PatternOfSuccess = StackNavigator(
  {
    PathOfSuccess: {
      screen: PathOfSuccess,
    },
    PathOfSuccess1: {
      screen: PathOfSuccess1,
    },
    PathOfSuccess3: {
      screen: PathOfSuccess3,
    },
    PathOfSuccess5: {
      screen: PathOfSuccess5,
    },
    PathOfSuccess7: {
      screen: PathOfSuccess7,
    },
    PathOfSuccess8: {
      screen: PathOfSuccess8,
    },
    PathOfSuccess9:{
      screen: PathOfSuccess9,
    },
    PathOfSuccess10:{
      screen: PathOfSuccess10,
    },
    PathOfSuccess11:{
      screen: PathOfSuccess11,
    },
  },
  {
    initialRouteName: 'PathOfSuccess',
    headerMode: 'none'
  }
);

export const FeedContact = StackNavigator(
  {
    Contact: {
      screen: Contact,
    },
    UserCall:{
      screen: CallInProgress,
      navigationOptions: {
        drawerLockMode: 'locked-closed'
      }
    },
    OutboundVideoCall:{
      screen: OutboundVideoCall,
      navigationOptions: {
        drawerLockMode: 'locked-closed'
      }
    }
  },
  {
    headerMode: 'none'
  }
);


export const SignedIn = DrawerNavigator(
  {
    Dashboard: {
      screen: Dashboard,
      navigationOptions: {
        drawerLabel: "Dashboard",
        drawerIcon: ({ tintColor }) => (
          <Icon
            name='ios-home-outline'
            size={24}
            style={{color: tintColor}}
          >
          </Icon>
        )
      }
    },
    Contact: {
      screen: FeedContact,
      navigationOptions: {
        drawerLabel: 'Contact',
        drawerIcon: ({tintColor}) => {
          return(
            <Icon
              name = 'ios-contact-outline'
              size = {24}
              style = {{color: tintColor}}
            >
            </Icon>
          );
        }
      }
    },
    Pathofsuccess: {
      screen: PatternOfSuccess,
      navigationOptions: {
        drawerLabel: "Path of success",
        drawerIcon: ({ tintColor }) => (
          <Icon
            name='ios-subway-outline'
            size={24}
            style={{color: tintColor}}
          >
          </Icon>
        )
      }
    },
    Historycall: {
      screen: HistoryCall,
      navigationOptions: {
        drawerLabel: "History Call",
        drawerIcon: ({ tintColor }) => (
          <Icon
            name='ios-call-outline'
            size={24}
            style={{color: tintColor}}
          >
          </Icon>
        )
      }
    }
  },
  {
    drawerWidth: 300,
    contentComponent: props => <SideDrawer nav={props} />
  }
);

export const createRootNavigator = (signedIn = false) => {
  return StackNavigator(
    {
      SignedIn:{
        screen: SignedIn,
        navigationOptions: {
          gesturesEnabled: false
        }
      },
      SignedOut:{
        screen: SignedOut,
        navigationOptions: {
          gesturesEnabled: false,
          headerMode: 'none'
        }
      }
    },
    {
      headerMode: "none",
      //mode: "modal",
      initialRouteName: signedIn ? "SignedIn" : "SignedOut",
    }
  );
};
