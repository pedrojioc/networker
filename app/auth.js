import { AsyncStorage } from "react-native";

export const USER_KEY = "auth-demo-key";

export const onSignIn = (userkey) => AsyncStorage.setItem("user_data", userkey);

export const onSignOut = () => {
  return AsyncStorage.removeItem("user_data")
    .then(() => {
      return true;
    })
}

// function onSignOut(){
//   return new Promise(() => {
//     AsyncStorage.removeItem("user_data")
//       .then((result) => {
//         return true;
//       })
//   })
// }

export const isSignedIn = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("user_data")
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};
