const URL = 'https://51jtmkgp0l.execute-api.us-west-2.amazonaws.com/prod/history/'

async function getContacts(data) {
  try {
    let response = await fetch('https://51jtmkgp0l.execute-api.us-west-2.amazonaws.com/prod/contacts/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: data[0]
      })
    })
    let responseJson = await response.json();
    
    return responseJson;
  } catch(error) {
    console.error(error);
  }
}

export { getContacts }