
const URL = 'https://51jtmkgp0l.execute-api.us-west-2.amazonaws.com/prod/login';

function  getData(idDevice,username,password){

  return fetch(URL, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: username,
        password: password,
        idDevice: idDevice,
      })
  })
    .then((response) => response.json())
    .then((responseJson) => {
      return responseJson;
    })
    .catch((error) => {
      console.error(error);
    })
}

export { getData }
