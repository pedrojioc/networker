function Call(dataCall) {

  const URL = 'https://a2284000.ngrok.io/rooms/create'
  return fetch(URL, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      userfrom: dataCall.userFrom,
      userto: dataCall.userTo,
    })
  })
    .then((response) => response.json())
    .then((responseJson) => {
      return responseJson;
    })
    .catch((error) => {
      console.error(error);
    })

}

export { Call }
