const URL = 'https://51jtmkgp0l.execute-api.us-west-2.amazonaws.com/prod/history/'

async function getHistoryCall(user) {
  try {
    let response = await fetch('https://51jtmkgp0l.execute-api.us-west-2.amazonaws.com/prod/history/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: user
      })
    })
    let responseJson = await response.json();
    console.log(responseJson);
    return responseJson;
  } catch(error) {
    console.error(error);
  }
}

function  getRecords(){
  return fetch(URL)
    .then(response => response.json())
    .then(response => JSON.parse(response))
    .then(response => {
      return response
    })
    // .then(response => response.map(record => {
    //   return {
    //     history_id: record.history_id,
    //     from: record.from,
    //     to: record.to,
    //     date: record.date,
    //     time: record.time,
    //     duration: record.duration,
    //     call_state: record.call_state
    //   }
    // }))
}

export { getRecords, getHistoryCall }