/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  ImageBackground,
  Image,
  TouchableOpacity
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNavWithBack from '../../core/HeaderNavWithBack'


export default class PathOfSuccess8 extends Component {

  buttonBack(){
    this.props.navigation.navigate('PathOfSuccess');
  }
  static navigationOptions = {
    /*headerTitle: 'Path of Success',
    headerTintColor: "white",
    headerStyle: {
       backgroundColor: '#4169FF',
       elevation: null,
     },
      left: ( <MaterialIcons name={'navigate-before'} onPress={ () => { goBack() } }  /> ),
      drawerLabel: 'Path of Success', */
      drawerIcon: ({tintColor}) => {
        return(
          <MaterialIcons
            name = 'send'
            size = {24}
            style = {{color: tintColor}}
          >
          </MaterialIcons>
        );
      },
      drawerLabel: 'Path of Success'
    }
  buttonNext(){
    this.props.navigation.navigate('PathOfSuccess9');
  }

  render() {
    return (
      <ImageBackground style={styles.imageback} source={require('../../assets/img/backgroundPath.png')}>
        <HeaderNavWithBack
          title="Path of Success"
          search={false}
          nav={this.props.navigation}
          back={'PathOfSuccess7'}
        />
        <View>
          <Text style={styles.tittle}>Succesful Meetings</Text>
          <Image style={styles.avatar} source={require('../../assets/img/Pattern/SuccessfulMeetings.png')}/>
          <View style={styles.content}>
            <Text style={styles.text}>
              Always plan your both meetings one close to the {"\n"}
              other to raise the chancs for the candidates to {"\n"}
              attend any ofthem and suggest extra {"\n"}
              appointments. Group meetings must be planned {"\n"}
              for 19:00h and they should not take more than {"\n"}
              two hours.
            </Text>
          </View>
          <View style={styles.viewnext}>
            <TouchableOpacity onPress={this.buttonNext.bind(this)}>
              <View style={styles.buttonnext}>
                <Text style={styles.next}>NEXT STEP→</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{height:20}}/>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  text:{
    color: '#fff',
    fontSize: 14,
    textAlign: "center",
    fontFamily: "Roboto-Regular"
  },
  tittle:{
    color: "white",
    fontSize: 34,
    marginTop: 30,
    alignSelf: "center",
    fontFamily: "Roboto-Bold"
  },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  avatar:{
    width: 190,
    height: 190,
    alignSelf: "center",
    marginTop: 40
  },
  content:{
    padding: 30
  },
  next:{
    color: '#fff',
    fontFamily:"Roboto-Bold",
    fontSize: 16
  },
  icondown:{
    width: 35,
    height: 22,
    marginLeft: 5
  },
  buttonnext:{
    alignSelf: "center",
    flexDirection: "row",
    marginBottom:20
  },
  back:{
    fontSize: 20,
    color: '#fff',
    marginLeft: 10,
    marginTop: -5,
    fontFamily: "Roboto-Regular",
    marginBottom: 8
  },
  viewback:{
    flexDirection: "row",
    marginTop: 30,
    marginLeft: 20
  },
  iconback:{
    width: 10,
    height: 15
  },
  viewbackTop:{
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 10
  },
  viewnext:{
    flexDirection: "row",
    alignSelf: "center",
  },
});
