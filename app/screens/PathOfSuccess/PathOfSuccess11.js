/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  ImageBackground,
  Image,
  TouchableOpacity
} from 'react-native';
import { StackNavigator, DrawerNavigator, NavigationActions } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNavWithBack from '../../core/HeaderNavWithBack'

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'PathOfSuccess'})
  ]
})

export default class PathOfSuccess11 extends Component {
  buttonBack(){
    this.props.navigation.navigate('PathOfSuccess');
  }
  static navigationOptions = {
    /*headerTitle: 'Path of Success',
    headerTintColor: "white",
    headerStyle: {
       backgroundColor: '#4169FF',
       elevation: null,
     },
      left: ( <MaterialIcons name={'navigate-before'} onPress={ () => { goBack() } }  /> ),
      drawerLabel: 'Path of Success',*/
      drawerIcon: ({tintColor}) => {
        return(
          <MaterialIcons
            name = 'send'
            size = {24}
            style = {{color: tintColor}}
          >
          </MaterialIcons>
        );
      },
      drawerLabel: 'Path of Success'
    }

  buttonNext(){
      this.props.navigation.dispatch(resetAction);
      //this.props.navigation.navigate('Dashboard');
  }

  render() {
    return (
      <ImageBackground style={styles.imageback} source={require('../../assets/img/backgroundPath.png')}>
        <HeaderNavWithBack
          title="Path of Success"
          search={false}
          nav={this.props.navigation}
          back={'PathOfSuccess10'}
        />
        <View>
          <Text style={styles.tittle}>Teach the Pattern</Text>
          <Image style={styles.avatar} source={require('../../assets/img/Pattern/TeachThePattern.png')}/>
          <View style={styles.content}>
            <Text style={styles.text}>
              Promoting the system is the best way to build a {"\n"}
              great business. Those who have the most {"\n"}
              successful business are the best system {"\n"}
              promoters.
            </Text>
          </View>
          <View style={styles.viewnext}>
            <TouchableOpacity onPress={this.buttonNext.bind(this)}>
              <View style={styles.buttonnext}>
                <Text style={styles.next}>FINISH</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  text:{
    color: '#fff',
    fontSize: 14,
    textAlign: "center",
    fontFamily: "Roboto-Regular"
  },
  tittle:{
    color: '#fff',
    fontSize: 34,
    marginTop: 30,
    alignSelf: "center",
    fontFamily: "Roboto-Bold"
  },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  avatar:{
    width: 190,
    height: 190,
    alignSelf: "center",
    marginTop: 40
  },
  content:{
    padding: 30
  },
  next:{
    color: '#fff',
    fontFamily:"Roboto-Bold",
    fontSize: 16
  },
  icondown:{
    width: 35,
    height: 22,
    marginLeft: 5
  },
  buttonnext:{
    alignSelf: "center",
    flexDirection: "row",
    marginTop: 5,
  },
  back:{
    fontSize: 20,
    color: '#fff',
    marginLeft: 10,
    marginTop: -5,
    fontFamily: "Roboto-Regular",
    marginBottom: 8
  },
  viewback:{
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 20
  },
  iconback:{
    width: 10,
    height: 15
  },
  viewbackTop:{
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 10
  },
  viewnext:{
    flexDirection: "row",
    alignSelf: "center",
    marginBottom: 20,
  },
});
