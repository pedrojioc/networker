/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  StatusBar
} from 'react-native';
import { StackNavigator, DrawerNavigator, NavigationActions } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNavWithBack from '../../core/HeaderNavWithBack'

export default class PathOfSuccess1 extends Component {


  buttonBack(){
      this.props.navigation.navigate('PathOfSuccess');
  }
  static navigationOptions = {
    /*headerTitle: 'Path of Success',
    headerTintColor: "white",
    headerStyle: {
       backgroundColor: '#4169FF',
       elevation: null,
     },
     left: ( <MaterialIcons name={'navigate-before'} onPress={ () => { goBack() } }  /> ),
     drawerLabel: 'Path of Success',*/
     drawerIcon: ({tintColor}) => {
       return(
         <MaterialIcons
           name = 'send'
           size = {24}
           style = {{color: tintColor}}
         >
         </MaterialIcons>
       );
     },
     drawerLabel: 'Path of Success'
      }
  buttonNext(){
      this.props.navigation.navigate('PathOfSuccess3');
  }

  render() {
    return (

      <ScrollView contentContainerStyle={styles.Container}>
        <View>
          <ImageBackground style={styles.imageback} source={require('../../assets/img/backgroundPath.png')}>
             <HeaderNavWithBack
               title="Path of Success"
               search={false}
               nav={this.props.navigation}
               back={'PathOfSuccess'}
             />
          <Text style={styles.tittle}>Map of dreams</Text>
          <Image style={styles.avatarMap} source={require('../../assets/img/Pattern/MapOfDreams.png')}/>
          <View style={styles.content}>
            <Text style={styles.text}>
              The Map of the Dreams is a blackboard you can {"\n"}
              make with photographs that represent your {"\n"}
              dreams. Choose the images that bestt describe {"\n"}
              your dreams, or add your own images from your {"\n"}
              cell phone
            </Text>
          </View>
          <View style={styles.down}>
            <Image style={styles.icondown} source={require('../../assets/img/icons/down.png')}/>
          </View>
          <View style={{height:30}}/>
          </ImageBackground>
        </View>
        <View style={{backgroundColor: '#495AFF'}}>
          <View>
            <View style={styles.imgtext}>
              <Image style={styles.avatar} source={require('../../assets/img/Pattern/Group_1.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  Type what you would wish for the future{"\n"}
                  if time and money weren{"''"}t a problem. Be {"\n"}
                  specific. Describe how different your life {"\n"}
                  would be in five years after reaching {"\n"}
                  some of your dreams and goals.
                </Text>
              </View>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.avatar2} source={require('../../assets/img/Pattern/Group_2.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  Take a look at them, touch themm, crop{"\n"}
                  specific image of your goals and put{"\n"}
                  them in a visible place for you, like a {"\n"}
                  mirror or the fridge. This will help you{"\n"}
                  remember your dreams daily and remind{"\n"}
                  you the motive to develop this busimess.
                </Text>
              </View>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.avatar3} source={require('../../assets/img/Pattern/Group_3.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  Are those objectives worth of time and{"\n"}
                  effort? Will you be happy because you{"\n"}
                  made the necessary efforts to obtain{"\n"}
                  them? Knowing what you want and when{"\n"}
                  do you want it will alllow your Direct{"\n"}
                  Distributor help you design a plan to {"\n"}
                  reach your goals.
                </Text>
              </View>
            </View>
            <TouchableOpacity onPress={this.buttonNext.bind(this)}>
              <View style={styles.buttonnext}>
                <Text style={styles.next}>NEXT STEP→</Text>
              </View>
            </TouchableOpacity>
            <View style={{height:30}}/>
            </View>
          </View>
        </ScrollView>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  text:{
    color: '#fff',
    fontSize: 14,
    textAlign: "center",
    fontFamily: "Roboto-Regular"
  },
  tittle:{
    color: '#fff',
    fontSize: 34,
    marginTop: 30,
    alignSelf: "center",
    fontFamily: "Roboto-Bold"
  },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  avatarMap:{
    width: 190,
    height: 190,
    alignSelf: "center",
    marginTop: 40
  },
  content:{
    padding: 30,
  },
  next:{
    color: '#fff',
  },
  icondown:{
    width: 35,
    height: 22,
    marginLeft: 5
  },
  buttonnext:{
    alignSelf: "center",
    flexDirection: "row",
    marginTop: 30,
    marginBottom: 20
  },
  back:{
    fontSize: 16,
    color: '#fff',
    marginLeft: 15,
    marginTop: -5
  },
  viewback:{
    flexDirection: "row",
    marginTop: 30,
    marginLeft: 20
  },
  iconback:{
    width: 10,
    height: 15
  },
  tittleview:{
    display: "flex",
    height: 55,
    marginTop:-20,
  },
  imgtext:{
    display: "flex",
  },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  avatar:{
    width: 262,
    height: 135,
    alignSelf: "center",
    marginTop: 60
  },
  content:{
    marginTop: 20,
  },
  next:{
    color: '#fff',
    fontFamily: "Roboto-Bold",
    fontSize: 16
  },
  icondown:{
    width: 35,
    height: 20,
    marginLeft: 10,
    alignSelf: "center"
  },
  buttonnext:{
    alignSelf: "center",
    flexDirection: "row",
    marginTop: 30
  },
  back:{
    fontSize: 20,
    color: '#fff',
    marginLeft: 10,
    marginTop: -5,
    fontFamily: "Roboto-Medium",
    marginBottom: 30
  },
  viewnext:{
    flexDirection: "row",
    alignSelf: "center",
    marginBottom: 20
  },
  viewbackTop:{
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 10
  },
  iconback:{
    width: 10,
    height: 20
  },
  avatar2:{
    width: 268,
    height: 133,
    alignSelf: "center",
    marginTop: 5
  },
  avatar3:{
    width: 261,
    height: 134,
    alignSelf: "center",
    marginTop: 5
  },
  down: {
    marginTop: 55,
    flex: 2,
  },


});
