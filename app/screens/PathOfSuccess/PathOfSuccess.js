/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  Image,
  TouchableOpacity,
  StatusBar,
  ImageBackground
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNavWithBack from '../../core/HeaderNavWithBack'



export default class PathOfSuccess extends Component {

  static navigationOptions = {
    headerMode: 'none',
    drawerLabel: 'Path of Success',
    drawerIcon: ({tintColor}) => {
      return(
        <MaterialIcons
          name = 'send'
          size = {24}
          style = {{color: tintColor}}
        >
        </MaterialIcons>
      );
    },
    drawerLabel: 'Path of Success'
   }

  buttonNext(){
      this.props.navigation.navigate('PathOfSuccess1');
  }

  render() {
    return (
      <ImageBackground style={styles.imageback} source={require('../../assets/img/backgroundPath.png')}>
        <StatusBar
           backgroundColor="#4169ff"
           barStyle="light-content"
         />
         <HeaderNavWithBack
           title="Path of Success"
           search={false}
           nav={this.props.navigation}
           back={'Dashboard'}
         />
        <View>
          <Text style={styles.tittle}>Pattern of Success</Text>
          <Image style={styles.avatar} source={require('../../assets/img/Pattern/PathOfSucces.png')}/>
          <View style={styles.content}>
            <Text style={styles.text}>
              The Pattern of success is a well-defined {"\n"} plan to
              grow your network and achieve {"\n"} success
              in your business.
            </Text>
          </View>
          <View>
            <TouchableOpacity onPress={this.buttonNext.bind(this)}>
              <View style={styles.buttonnext}>
                <Text style={styles.next}>NEXT STEP→</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#ffffff',
    },
    text:{
      color: '#fff',
      marginTop: 30,
      fontSize: 16,
      textAlign: "center",
      fontFamily: "Roboto-Regular",
    },
    tittle:{
      color: '#fff',
      fontSize: 32,
      marginTop: 30,
      alignSelf: "center",
      fontFamily: "Roboto-Bold"
    },
    imageback:{
      flex:1,
      width:null,
      height:null
    },
    avatar:{
      width: 200,
      height: 189,
      alignSelf: "center",
      marginTop: 50
    },
    content:{
      padding: 30
    },
    next:{
      color: '#fff',
      fontFamily: "Roboto-Bold",
      fontSize: 16
    },
    iconnext:{
      width: 10,
      height: 20,
      marginLeft: 10
    },
    buttonnext:{
      alignSelf: "center",
      flexDirection: "row",
    }
});
