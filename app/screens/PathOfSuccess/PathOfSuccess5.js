/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNavWithBack from '../../core/HeaderNavWithBack'

export default class PathOfSuccess5 extends Component {

  buttonBack(){
    this.props.navigation.navigate('PathOfSuccess');
  }
  static navigationOptions = {
  /*  headerTitle: 'Path of Success',
    headerTintColor: "white",
    headerStyle: {
       backgroundColor: '#4169FF',
       elevation: null,
     },
      left: ( <MaterialIcons name={'navigate-before'} onPress={ () => { goBack() } }  /> ),
      drawerLabel: 'Path of Success',*/
      drawerIcon: ({tintColor}) => {
        return(
          <MaterialIcons
            name = 'send'
            size = {24}
            style = {{color: tintColor}}
          >
          </MaterialIcons>
        );
      },
      drawerLabel: 'Path of Success'
    }

  buttonNext(){
      this.props.navigation.navigate('PathOfSuccess7');
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <ImageBackground style={styles.imageback} source={require('../../assets/img/backgroundPath.png')}>
            <HeaderNavWithBack
              title="Path of Success"
              search={false}
              nav={this.props.navigation}
              back={'PathOfSuccess3'}
            />
            <View>
              <Text style={styles.tittle}>Make a list</Text>
              <Image style={styles.avatarMakeaList} source={require('../../assets/img/Pattern/MakeAList.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  The key to success is people. Make a list of the {"\n"}
                  names. Take two hours exclusively to prepare a list {"\n"}
                  with the names of all the people that you know, {"\n"}
                  those who live far away, those who you don{"''"}t{"\n"}
                  mewwt often, etc. (Marriages can make this list {"\n"}
                  together)
                </Text>
              </View>
              <View style={styles.buttonnext}>
                <Image style={styles.icondown} source={require('../../assets/img/icons/down.png')}/>
              </View>
              <View style={{height:50}}>
              </View>
            </View>
          </ImageBackground>
        </View>
        <View style={{backgroundColor:'#495AFF'}}>
          <View style={styles.content}>
            <Text style={styles.text}>
              We can clasify people according to {"\n"}
              the relationships we have with them, {"\n"}
              begin your list the same way.
            </Text>
          </View>
          <View style={styles.imgtext}>
            <Image style={styles.avatar} source={require('../../assets/img/Pattern/Group_11.png')}/>
          </View>
          <View style={styles.imgtext}>
            <View style={styles.viewSubTittle}>
              <Text style={styles.subTittle}>1. Inmediate candidates.</Text>
            </View>
          <View style={styles.content}>
            <Text style={styles.text}>
              Family, friends and people you know, neighbours {"\n"}
              (old and new), people from your job, classmates, {"\n"}
              every person you know that wants to earn money, {"\n"}
              dont{"'"}t judge.
            </Text>
          </View>
          </View>
          <View style={styles.imgtext}>
            <View style={styles.viewSubTittle}>
              <Text style={styles.subTittle}>2. Indirect candidates.</Text>
            </View>
            <View style={styles.content}>
              <Text style={styles.text}>
                Those you know through your children, school, {"\n"}
                cars, personal or domestic services, army, clubs, {"\n"}
                pets, shopping, etc. Take into consideration {"\n"}
                groups and not only the names of the people for {"\n"}
                the list, keep assing names as fast as you can {"\n"}
                think of them.
              </Text>
            </View>
          </View>
          <View style={styles.imgtext}>
            <View style={styles.viewSubTittle}>
              <Text style={styles.subTittle}>3. Unknow people.</Text>
            </View>
            <View style={styles.content}>
              <Text style={styles.text}>
                Develop the ability of projecting posible candidates {"\n"}
                onto all those around, make it a habit. {"\n"}
                Visualize people as if they were already doing the {"\n"}
                business or as if they wanted to do it. Learn how to {"\n"}
                listen, if you pay attention when someone is talking {"\n"}
                you can easily know which moment that person is {"\n"}
                open to discus about the business.
              </Text>
            </View>
          </View>
          <View style={styles.viewnext}>
            <TouchableOpacity onPress={this.buttonNext.bind(this)}>
              <View style={styles.buttonnext}>
                <Text style={styles.next}>NEXT STEP→</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text:{
    color: '#fff',
    fontSize: 14,
    textAlign: "center",
    fontFamily: "Roboto-Regular"
  },
  tittle:{
    color: "white",
    fontSize: 34,
    marginTop: 30,
    alignSelf: "center",
    fontFamily: "Roboto-Bold"
  },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  avatarMakeaList:{
    width: 190,
    height: 190,
    alignSelf: "center",
    marginTop: 40
  },
  content:{
    padding: 30
  },
  next:{
    color: '#fff',
  },
  icondown:{
    width: 35,
    height: 22,
    marginLeft: 5
  },
  buttonnext:{
    alignSelf: "center",
    flexDirection: "row",
    marginTop: 10,
  },
  back:{
    fontSize: 20,
    color: '#fff',
    marginLeft: 10,
    marginTop: -5,
    fontFamily: "Roboto-Regular",
    marginBottom: 8
  },
  viewback:{
    flexDirection: "row",
    marginTop: 30,
    marginLeft: 20
  },
  iconback:{
    width: 10,
    height: 15
  },
  viewbackTop:{
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 10
  },
subTittle:{
  color: '#fff',
  fontSize: 26,
  marginTop: 10,
  alignSelf: "center",
  fontFamily: "Roboto-Bold"
},
tittleview:{
  display: "flex",
  height: 55,
},
imgtext:{
  display: "flex",
},
imageback:{
  flex:1,
  width:null,
  height:null
},
avatar:{
  width: 150,
  height: 239,
  alignSelf: "center",
  marginTop: 70
},
content:{
  marginTop: 10,
},
next:{
  color: '#fff',
  fontFamily: "Roboto-Bold",
  fontSize: 16
},
icondown:{
  width: 35,
  height: 20,
  marginLeft: 10,
  alignSelf: "center"
},
buttonnext:{
  alignSelf: "center",
  flexDirection: "row",
  marginTop: 30
},
back:{
  fontSize: 20,
  color: "white",
  marginLeft: 10,
  marginTop: -5,
  fontFamily: "Roboto-Regular",
  marginBottom: 8
},
viewnext:{
  flexDirection: "row",
  alignSelf: "center",
  marginBottom: 20,
  marginTop: 10,
},
viewbackTop:{
  flexDirection: "row",
  marginTop: 15,
  marginLeft: 10
},
iconback:{
  width: 10,
  height: 20
},
avatar2:{
  width: 258,
  height: 128,
  alignSelf: "center",
  marginTop: 5
},
avatar3:{
  width: 252,
  height: 125,
  alignSelf: "center",
  marginTop: 5
},
avatar4:{
  width: 252,
  height: 125,
  alignSelf: "center",
  marginTop: 5
},
avatar5:{
  width: 251,
  height: 126,
  alignSelf: "center",
  marginTop: 5
},
avatar6:{
  width: 258,
  height: 127,
  alignSelf: "center",
  marginTop: 5
},
});
