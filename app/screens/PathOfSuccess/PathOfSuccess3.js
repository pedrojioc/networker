/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNavWithBack from '../../core/HeaderNavWithBack'


export default class PathOfSuccess3 extends Component {
  buttonBack(){
      this.props.navigation.navigate('PathOfSuccess');
  }
  static navigationOptions = {
    /* headerTitle: 'Path of Success',
    headerTintColor: '#fff',
    headerStyle: {
       backgroundColor: '#4169FF',
       elevation: null,
     },
     left: ( <MaterialIcons name={'navigate-before'} onPress={ () => { goBack() } }  /> ),
     drawerLabel: 'Path of Success', */
     drawerIcon: ({tintColor}) => {
       return(
         <MaterialIcons
           name = 'send'
           size = {24}
           style = {{color: tintColor}}
         >
         </MaterialIcons>
       );
     },
     drawerLabel: 'Path of Success'
   }

  buttonNext(){
      this.props.navigation.navigate('PathOfSuccess5');
  }

  render() {
    return (
        <ScrollView>
          <ImageBackground style={styles.imageback} source={require('../../assets/img/backgroundPath.png')}>
            <View>
              <HeaderNavWithBack
                title="Path of Success"
                search={false}
                nav={this.props.navigation}
                back={'PathOfSuccess1'}
              />
              <Text style={styles.tittle}>Commitments</Text>
              <Image style={styles.avatarCommitments} source={require('../../assets/img/Pattern/Commitments.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  To conquer your dreams you must make{"\n"}
                  commitments and be willing to fulfill them. Add {"\n"}
                  the commitments you wull make to grow your{"\n"}
                  business exponentially {"\n"}
                  cell phone
                </Text>
              </View>
              <View>
                <Image style={styles.icondown} source={require('../../assets/img/icons/down.png')}/>
              </View>
              <View style={{height:30}}/>
            </View>
          </ImageBackground>
          <View style={{backgroundColor:'#495AFF'}}>
            <View style={styles.imgtext}>
              <Image style={styles.avatar} source={require('../../assets/img/Pattern/Group_4.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  Be firmly determind to complete all the{"\n"}
                  neccessary activities to reach your dreams.{"\n"}
                  Commit yourself to your sponsor and all{"\n"}
                  your sponsor lines. Commit yourself with {"\n"}
                  all your sponsored ones as well and {"\n"}
                  stabilish a responsible and mutually {"\n"}
                  supportive relation with them.
                </Text>
              </View>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.avatar2} source={require('../../assets/img/Pattern/Group_5.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  You must take all the tasks that your{"\n"}
                  business require seriously and {"\n"}
                  professionally, such as: look constantly{"\n"}
                  the prospects and new markets, organize{"\n"}
                  meetings and present the plan{"\n"}
                  constantly, organize all the stages of {"\n"}
                  your growth and all the group{"'"}s activities {"\n"}
                  carefully, support and guide the {"\n"}
                  members of your network, offer an  {"\n"}
                  excellent service with all the customers {"\n"}
                  to avoid disloyal competence or {"\n"}
                  unethical practices towards the business {"\n"}
                  and always have in your hand the tools {"\n"}
                  yo develop your business.
                </Text>
              </View>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.avatar3} source={require('../../assets/img/Pattern/Group_6.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  Almost everything that you buy today{"\n"}
                  can and must be purchased from your{"\n"}
                  own business. This is called “product {"\n"}
                  loyalty.” Check all the catalogs carefully {"\n"}
                  in order to get familiar with the products {"\n"}
                  and services available fot you on your {"\n"}
                  business.
                </Text>
              </View>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.avatar4} source={require('../../assets/img/Pattern/Group_8.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  Almost everything that you buy today{"\n"}
                  can and must be purchased from your{"\n"}
                  own business. This is called “product {"\n"}
                  loyalty.” Check all the catalogs carefully {"\n"}
                  in order to get familiar with the products {"\n"}
                  and services available fot you on your {"\n"}
                  business.
                </Text>
              </View>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.avatar5} source={require('../../assets/img/Pattern/Group_9.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  Knowledge is power and successful {"\n"}
                  leaders in the business have developed a {"\n"}
                  system that provides you with the{"\n"}
                  knowledge that you will need to {"\n"}
                  successfully build and duplicate this {"\n"}
                  business. The education system has:{"\n"}
                  -CDs and videos{"\n"}
                  -Books{"\n"}
                  -Seminaries and conventions.
                </Text>
              </View>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.avatar6} source={require('../../assets/img/Pattern/Group_10.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  As you learn and practices, you develop {"\n"}
                  the necessary skills build your {"\n"}
                  business. Therefore, you must develop {"\n"}
                  certain behaviors and perform certain {"\n"}
                  activities which will become habits able {"\n"}
                  to help you build a wealthy business with {"\n"}
                  constant practice.
                </Text>
              </View>
            </View>
            <View style={styles.viewnext}>
              <TouchableOpacity onPress={this.buttonNext.bind(this)}>
                <View style={styles.buttonnext}>
                  <Text style={styles.next}>NEXT STEP→</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{height:20}}/>
          </View>
        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  text:{
    color: '#fff',
    fontSize: 14,
    textAlign: "center",
    fontFamily: "Roboto-Regular"
  },
  tittle:{
    color: '#fff',
    fontSize: 34,
    marginTop: 30,
    alignSelf: "center",
    fontFamily: "Roboto-Bold",
  },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  avatarCommitments:{
    width: 232,
    height: 206,
    alignSelf: "center",
    marginTop: 40
  },
  content:{
    padding: 30
  },
  next:{
    color: '#fff',
    fontSize: 16,
    fontFamily: "Roboto-Bold"
  },
  icondown:{
    width: 35,
    height: 22,
    marginLeft: 5,
    alignSelf: "center"
  },
  buttonnext:{
    alignSelf: "center",
    flexDirection: "row",
    marginTop: 5,
    marginBottom: 15
  },
  back:{
    fontSize: 20,
    color: '#fff',
    marginLeft: 15,
    marginTop: -5,
    fontFamily: "Roboto-Medium"
  },
  viewback:{
    flexDirection: "row",
    marginTop: 30,
    marginLeft: 20
  },
  iconback:{
    width: 10,
    height: 15,
    alignSelf: "center"
  },
  avatar2:{
    width: 258,
    height: 128,
    alignSelf: "center",
    marginTop: 5
  },
  avatar3:{
    width: 252,
    height: 125,
    alignSelf: "center",
    marginTop: 5
  },
  avatar4:{
    width: 252,
    height: 125,
    alignSelf: "center",
    marginTop: 5
  },
  avatar5:{
    width: 251,
    height: 126,
    alignSelf: "center",
    marginTop: 5
  },
  avatar6:{
    width: 258,
    height: 127,
    alignSelf: "center",
    marginTop: 5
  },
  avatar:{
    width: 252,
    height: 130,
    alignSelf: "center",
    marginTop: 50
  },
});
