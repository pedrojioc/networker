/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNavWithBack from '../../core/HeaderNavWithBack'

export default class PathOfSuccess9 extends Component {

  buttonBack(){
    this.props.navigation.navigate('PathOfSuccess');
  }
  static navigationOptions = {
    /*headerTitle: 'Path of Success',
    headerTintColor: "white",
    headerStyle: {
       backgroundColor: '#4169FF',
       elevation: null,
     },
      left: ( <MaterialIcons name={'navigate-before'} onPress={ () => { goBack() } }  /> ),
      drawerLabel: 'Path of Success',*/
      drawerIcon: ({tintColor}) => {
        return(
          <MaterialIcons
            name = 'send'
            size = {24}
            style = {{color: tintColor}}
          >
          </MaterialIcons>
        );
      },
      drawerLabel: 'Path of Success'
    }

  buttonNext(){
      this.props.navigation.navigate('PathOfSuccess10');
  }

  render() {
    return (
      <ScrollView>
        <View>
          <ImageBackground style={styles.imageback} source={require('../../assets/img/backgroundPath.png')}>
            <HeaderNavWithBack
              title="Path of Success"
              search={false}
              nav={this.props.navigation}
              back={'PathOfSuccess8'}
            />
            <View>
              <Text style={styles.tittle}>Tracking</Text>
              <Image style={styles.avatar} source={require('../../assets/img/Pattern/Tracking.png')}/>
              <View style={styles.content}>
                <Text style={styles.text}>
                  Basically, this step is focused on solving doubts {"\n"}
                  and emphasize on the importane of seeing more {"\n"}
                  information. In this meeting you can also explain {"\n"}
                  the importance of attending a seminary and a {"\n"}
                  convention for the development of business.
                </Text>
              </View>
              <View style={styles.buttonnext}>
                <Image style={styles.icondown} source={require('../../assets/img/icons/down.png')}/>
              </View>
              <View style={{height:40}}>
              </View>
            </View>
          </ImageBackground>
        </View>
        <View style={{backgroundColor:'#495AFF'}}>
          <View>
            <View style={styles.imgtext}>
              <Image style={styles.img} source={require('../../assets/img/Pattern/img1.png')}/>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.img} source={require('../../assets/img/Pattern/img2.png')}/>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.img} source={require('../../assets/img/Pattern/img3.png')}/>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.img} source={require('../../assets/img/Pattern/img4.png')}/>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.img} source={require('../../assets/img/Pattern/img5.png')}/>
            </View>
            <View style={styles.imgtext}>
              <Image style={styles.img} source={require('../../assets/img/Pattern/img6.png')}/>
            </View>
          </View>
          <View>
            <TouchableOpacity onPress={this.buttonNext.bind(this)}>
              <View style={styles.buttonnext}>
                <Text style={styles.next}>NEXT STEP→</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{height:20}}/>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  text:{
    color: '#fff',
    fontSize: 14,
    textAlign: "center",
    fontFamily: "Roboto-Regular"
  },
  tittle:{
    color:'#fff',
    fontSize: 34,
    marginTop: 30,
    alignSelf: "center",
    fontFamily: "Roboto-Bold"
  },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  avatar:{
    width: 190,
    height: 190,
    alignSelf: "center",
    marginTop: 40
  },
  content:{
    padding: 30
  },
  next:{
    color: '#fff',
    fontFamily:"Roboto-Bold",
    fontSize: 16
  },
  icondown:{
    width: 35,
    height: 22,
    marginLeft: 5
  },
  buttonnext:{
    alignSelf: "center",
    flexDirection: "row",
  },
  back:{
    fontSize: 20,
    color: '#fff',
    marginLeft: 10,
    marginTop: -5,
    fontFamily: "Roboto-Regular",
    marginBottom: 8
  },
  viewback:{
    flexDirection: "row",
    marginTop: 30,
    marginLeft: 20
  },
  iconback:{
    width: 10,
    height: 15
  },
  viewbackTop:{
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 10
  },
  viewnext:{
    flexDirection: "row",
    alignSelf: "center",
  },
  imgtext:{
    alignItems: "center",
    marginBottom: 20
  }
});
