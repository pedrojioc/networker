/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  ImageBackground,
  Image,
  TouchableOpacity
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNavWithBack from '../../core/HeaderNavWithBack'

export default class PathOfSuccess10 extends Component {

  buttonBack(){
    this.props.navigation.navigate('PathOfSuccess');
  }
  static navigationOptions = {
  /*  headerTitle: 'Path of Success',
    headerTintColor: "white",
    headerStyle: {
       backgroundColor: '#4169FF',
       elevation: null,
     },
      left: ( <MaterialIcons name={'navigate-before'} onPress={ () => { goBack() } }  /> ),
      drawerLabel: 'Path of Success',*/
      drawerIcon: ({tintColor}) => {
        return(
          <MaterialIcons
            name = 'send'
            size = {24}
            style = {{color: tintColor}}
          >
          </MaterialIcons>
        );
      },
      drawerLabel: 'Path of Success'
    }

  buttonNext(){
      this.props.navigation.navigate('PathOfSuccess11');
  }

  render() {
    return (
      <ImageBackground style={styles.imageback} source={require('../../assets/img/backgroundPath.png')}>
        <HeaderNavWithBack
          title="Path of Success"
          search={false}
          nav={this.props.navigation}
          back={'PathOfSuccess9'}
        />
        <View>
          <Text style={styles.tittle}>Progress Tracking</Text>
          <Image style={styles.avatar} source={require('../../assets/img/Pattern/ProgressTracking.png')}/>
          <View style={styles.content}>
            <Text style={styles.text}>
              It is essential to continuously check your growth {"\n"}
              in order to know how mucho you have advanced {"\n"}
              and how far you are from reaching your goals and {"\n"}
              things you promised yo achieve every month. {"\n"}
              In this way you will know about your situation, if {"\n"}
              you are doing things correctly and what do you {"\n"}
              need to do to improve your advanced.
            </Text>
          </View>
          <View style={styles.viewnext}>
            <TouchableOpacity onPress={this.buttonNext.bind(this)}>
              <View style={styles.buttonnext}>
                <Text style={styles.next}>NEXT STEP→</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{height:30}}/>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  text:{
    color: '#fff',
    fontSize: 14,
    textAlign: "center",
    fontFamily: "Roboto-Regular"
  },
  tittle:{
    color: '#fff',
    fontSize: 34,
    marginTop: 20,
    alignSelf: "center",
    fontFamily: "Roboto-Bold"
  },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  avatar:{
    width: 190,
    height: 190,
    alignSelf: "center",
    marginTop: 30
  },
  content:{
    padding: 30
  },
  next:{
    color:'#fff',
    fontFamily:"Roboto-Bold",
    fontSize: 16
  },
  icondown:{
    width: 35,
    height: 22,
    marginLeft: 5
  },
  buttonnext:{
    alignSelf: "center",
    flexDirection: "row",
  },
  back:{
    fontSize: 20,
    color: '#fff',
    marginLeft: 10,
    marginTop: -5,
    fontFamily: "Roboto-Regular",
    marginBottom: 8
  },
  viewback:{
    flexDirection: "row",
    marginLeft: 20
  },
  iconback:{
    width: 10,
    height: 15
  },
  viewbackTop:{
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 10
  },
  viewnext:{
    flexDirection: "row",
    alignSelf: "center",

  },
});
