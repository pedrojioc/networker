/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  Image,
  TouchableHighlight
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNav from '../core/HeaderNav'


export default class PathOfSuccess extends Component {
  static navigationOptions = {
    tabBarLabel: 'Screen 3',
    drawerIcon: ({tintColor}) => {
      return(
        <MaterialIcons
          name = 'send'
          size = {24}
          style = {{color: tintColor}}
        >
        </MaterialIcons>
      );
    },
    drawerLabel: 'Path of Success'
  }

  render() {
    return (
      <View style={styles.container}>
        <HeaderNav
          title="Path of Sucees"
          search={false}
          nav={this.props.navigation}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  }
});
