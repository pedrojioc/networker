/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  Image,
  ImageBackground
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'


export default class CallInProgress extends Component {

  render(){
    return (
      <Image style={styles.imageback} source={require('../assets/img/backgroundlogin.png')}>
      <View>
        <Image style={styles.avatar} source={require('../assets/img/avatar.png')}/>
        <Text style={styles.contact}> Jon Snow </Text>
        <View style={styles.subview}>
            <TouchableHighlight onPress={this._onPressButton}>
              <Image style={styles.oncall} source={require('../assets/img/oncall.png')}/>
            </TouchableHighlight>
            <TouchableHighlight onPress={this._onPressButton}>
              <Image style={styles.endcall} source={require('../assets/img/endcall.png')}/>
            </TouchableHighlight>
        </View>
      </View>
      </Image>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  contact:{
    fontFamily: "Avenir Next",
    fontSize: 38,
    alignSelf: "center",
    marginTop: 15,
    color: "white"
  },
  avatar:{
    width: 120,
    height: 120,
    alignSelf: "center",
    marginTop: 100
  },
  subview:{
    flexDirection: "row",
    alignSelf: "center"
  },
  endcall:{
    alignSelf: "center",
    marginTop: 150,
    marginLeft: 120,
  },
  oncall:{
    alignSelf: "center",
    marginTop: 150,
    marginLeft: 0
  }
});
