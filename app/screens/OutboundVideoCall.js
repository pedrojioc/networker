import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  Image,
  ImageBackground,
  StatusBar,
  TouchableHighlight
} from 'react-native';

export default class OutboundVideoCall extends Component{
	constructor(props) {
		super(props);
		
	}

	render(){
		return(
			<Image style={styles.imgContact} source={require('../assets/img/backgroud_out_call.png')}>
				<View>
					<StatusBar
					  translucent
					  backgroundColor="rgba(0, 0, 0, 0.30)"
					  animated
					/>
				</View>
				<View style={styles.content}>
					<View style={styles.contentAvatar}>
						<Image style={styles.avatar} source={require('../assets/img/small_avatar.png')} />
						<Text style={styles.textInfo}>VIDEO CALL VIA NETWORKET PRO</Text>
						<Text style={styles.name}>Sofia Deer</Text>
						<Text style={styles.typeCall}>OUTGOIN</Text>
					</View>
					<View style={styles.contentButton}>
						<TouchableHighlight onPress={() => this.props.navigation.navigate('Contact')}>
							<Image style={styles.endCall} source={require('../assets/img/icons/endcall.png')} />
						</TouchableHighlight>
					</View>
				</View>
			</Image>
		);
	}

}
const styles = StyleSheet.create({
	imgContact:{
		flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover'
	},
	content:{
		flex:1,
		backgroundColor: 'rgba(0, 0, 0, 0.55)',
		paddingTop: 80,
		paddingBottom: 20,
		justifyContent: 'space-between'
	},
	contentAvatar:{
		justifyContent: 'center',
		alignItems: 'center'
	},
	avatar:{
		width: 70,
		height: 70
	},
	contentButton:{
		flexDirection: 'row',
		justifyContent: 'center'
	},
	endCall:{
		width: 65,
		height: 65
	},
	textInfo:{
		color: '#ffffff',
		fontWeight: '100',
		marginTop: 25
	},
	name:{
		color: '#ffffff',
		fontSize: 28,
		fontWeight: 'normal'
	},
	typeCall:{
		color: '#ffffff',
		fontSize: 12,
		fontWeight: '100'
	}
});