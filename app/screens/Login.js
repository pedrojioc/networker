import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  Alert,
  Image,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView
} from 'react-native';
import HeaderNav from '../core/HeaderNav'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import OneSignal from 'react-native-onesignal';
import SnackBar from 'react-native-snackbar-dialog';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { FormLabel,
         FormInput, } from 'react-native-elements'
import { NavigationActions, StackNavigator } from 'react-navigation'
import { getData } from '../api-client/Login'
import { onSignIn } from '../auth'



/*const navigateAction = NavigationActions.navigate({
  routeName: 'Inicio',
  params: {},
  action: NavigationActions.navigate({routeName: 'Dashboard'})
})*/

export default class Login extends Component {

/*-----------------------------------------------------------Constructor------------------------------------------------------------*/
    constructor(props) {
      super(props);
      this.state = { text: 'Useless Placeholder',
                     snackbar: false,
                     login: false,
                     loginPressed: false,
                    }
      this.inputs = {
        username: '',
        password: '',
      };
      this.onIds = this.onIds.bind(this);
      this.onReceived = this.onReceived.bind(this);
    };
/*-----------------------------------------------------------Constructor------------------------------------------------------------*/
/*-----------------------------------------------------------ONE SIGNAL------------------------------------------------------------*/
    componentWillMount() {
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('registered', this.onRegistered);
        OneSignal.addEventListener('ids', this.onIds);
    }
    componentWillUnmount() {
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
        OneSignal.removeEventListener('registered', this.onRegistered);
        OneSignal.removeEventListener('ids', this.onIds);
    }
    onReceived(notification) {
        console.log("Notification received: ", notification);
    }
    onOpened(openResult) {
      console.log('Message: ', openResult.notification.payload.body);
      console.log('Data: ', openResult.notification.payload.additionalData);
      console.log('isActive: ', openResult.notification.isAppInFocus);
      console.log('openResult: ', openResult);
    }
    onRegistered(notifData) {
      console.log("Device had been registered for push notifications!", notifData);
    }
    onIds(device) {
		  console.log('Device info: ', device);
      this.setState({deviceInfo: device})
    }
/*-----------------------------------------------------------ONE SIGNAL------------------------------------------------------------*/
  showModalError = () =>{
    Alert.alert(
      'Error',
      'Something has failed',
      [
        {text: 'Try now', onPress: () => console.log('Ask me later pressed')},
      ],
      { cancelable: false }
    )
  }

  validateForm(){
    if(this.inputs.username.trim().length == 0){
      this.showAlert('Please, enter username');
      this.formInput.shake();
      return false;
    }
    if(this.inputs.password.trim().length == 0){
      this.showAlert('Please, enter Password');
      this.formInput.shake();
      return false;
    }
    return true;
  }

  showAlert(mensaje){
    SnackBar.show(mensaje, {
      style: { marginBottom: 20 },
      backgroundColor: 'rgba(255, 2, 35, 0.6))',
      textColor: 'white',
      tapToClose: true,
    });
  }

    //METODO DE VALIDA login
    validateLogin(status,username,userkey,idDevice){
        //console.warn(status);
        switch (status) {
          case 1:
            //this.loginSuccess(username,userkey);
            onSignIn(userkey);
            this.setState({ loginPressed: false });
            this.props.navigation.navigate('Dashboard');
          break;
          case 7,8:
            this.showAlert('The username or password is incorrect');
            this.setState({ loginPressed: false });
          break;
        }
    }
//presiona botón login, Valida campos vacios y consulta con la  API
    buttonLogin(){
      if(!this.validateForm()){
        return;
      }
      this.setState({ loginPressed: true });
      try {
        var idDevice = this.state.deviceInfo.userId.toString();
        var username = this.inputs.username;
        var password = this.inputs.password;
        getData(idDevice, username, password)
          .then(responseJson => {
            var status = responseJson.status;
            var userkey = username+"~"+responseJson.token+"~"+idDevice;  //value
            //llamada al metodo
            this.validateLogin(status,username,userkey);
          })
      } catch (e) {
        console.log('Error Login'+e);
        this.setState({ loginPressed: false });
        this.showModalError();
        console.warn(e)
      }
    }

    loginSuccess(username,userkey) {
      onSignIn(userkey);
      this.props.navigation.navigate('Dashboard');
    }

    renderButtonLogin(){
      return(
        <TouchableOpacity style={styles.button} onPress={() => this.buttonLogin(this)}>
          <Text style={styles.textBlue} >LOG IN TO NETWORKER PRO</Text>
        </TouchableOpacity>
      );
    }

    renderLoadingIdicator(){
      return(
        <ActivityIndicator
          animating = {true}
          color = "white"
          size = "large"
          style = {styles.activityIndicator}
        />
      );
    }

    render() {
      const animating = this.state.animating
      const button = this.state.loginPressed ? this.renderLoadingIdicator() : this.renderButtonLogin()
      return(
          <Image style={styles.imageback} source={require('../assets/img/backgroundlogin.png')}>
          <KeyboardAwareScrollView
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={styles.container}
                scrollEnabled={false}>
              <Text style={styles.logo}>Welcome to</Text>
              <Text style={styles.summary}>Networker Pro</Text>
              <Text style={styles.summarynext}>Sign in to continue</Text>
              <View>
                  <FormLabel labelStyle={{color:"white", marginBottom: -10, fontSize:14}}>Username</FormLabel>
                  <FormInput
                    inputStyle={{color:"white", }}
                    underlineColorAndroid= "white"
                    ref= {ref => this.formInput = ref}
                    onChangeText={(text) => {
                    this.inputs.username = text;
                    }}/>
                  <FormLabel labelStyle={{color:"white", marginBottom: -10, fontSize:14}}>Password</FormLabel>
                  <FormInput
                      inputStyle={{color:"white" }}
                      underlineColorAndroid= "white"
                      secureTextEntry={true}
                      ref= {ref => this.formInput = ref}
                      onChangeText={(text) => {
                        this.inputs.password = text;
                  }}/>
                  </View>
                  <View style={{height:38}}/>
                  {button}
              </KeyboardAwareScrollView>
          </Image>
      );
    }
}

const SimpleApp = StackNavigator({
  Login: { screen: Login },
});

const styles = StyleSheet.create({
  container:{
    marginLeft:34,
    marginRight:34,
    marginTop:115,
    justifyContent: "space-between",
  },
  logo:{
    fontSize:22,
    color:"white",
    backgroundColor: "transparent",
    fontFamily: "Roboto-Regular",
    margin: "auto",
  },
  summary:{
      fontSize:35,
      color:"white",
      backgroundColor: "transparent",
      fontFamily: 'Roboto-Bold'
  },
  summarynext:{
    fontSize:21,
    color:"white",
    backgroundColor: "transparent",
    fontFamily: "Roboto-Regular"
  },
  button: {
    marginTop:25,
    width:270,
    padding:15,
    flexDirection:'row',
    justifyContent: 'center',
    backgroundColor: "white",
    borderRadius: 4,
    alignSelf:'center',
  },
  text: { color: "white", textAlign: "center" },
  textBlue: {color: '#495AFF', backgroundColor: 'transparent', fontWeight: "500", fontFamily: "Roboto-Regular"},
  textLabel: { tintColor: "white" },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  activityIndicator: {
   justifyContent: 'center',
   alignItems: 'center',
}
});
