/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import OneSignal from 'react-native-onesignal';
import {
  Text,
  StyleSheet,
  View,
  Button
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import HeaderNav from '../core/HeaderNav'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { AsyncStorage } from "react-native"


export default class Dashboard extends Component {

  constructor(props){
      super(props);
      this.onOpened = this.onOpened.bind(this);
  }


  static navigationOptions = {
    tabBarLabel: 'Screen 1',
    drawerIcon: ({tintColor}) => {
      return(
        <MaterialIcons
          name='dashboard'
          size={24}
          style={{color: tintColor}}
        >
        </MaterialIcons>
      );
    },
    drawerLabel: 'Dashboard'
  }

  componentWillMount() {
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
  }

  componentWillUnmount() {
      OneSignal.removeEventListener('received', this.onReceived);
      OneSignal.removeEventListener('opened', this.onOpened);
  }

  onReceived(notification) {
      console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
      console.log('Message: ', openResult.notification.payload.body);
      console.log('Data: ', openResult.notification.payload.additionalData);

      var roomId = openResult.notification.payload.additionalData.roomId;

      if(roomId != ""){
        console.log("Entroo");
        this.props.navigation.navigate("CallInProgress", {roomId: roomId});
      }

      console.log('isActive: ', openResult.notification.isAppInFocus);
      console.log('openResult: ', openResult);
  }

  render() {

    return (
      <View style={styles.container}>
        <HeaderNav
          title="Dashboard"
          search={false}
          nav={this.props.navigation}
        />
        <Text>Holaaaaaa</Text>
        <Button
          onPress={() => AsyncStorage.removeItem("user_data")}
          title="Logout"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  }
});

//AppRegistry.registerComponent('Networker', () => Networker);
