/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  ImageBackground,
  Image,
  FlatList,
  ActivityIndicator
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNav from '../core/HeaderNav'
import HistoryBox from '../core/HistoryBox'
import { getRecords, getHistoryCall } from '../api-client/HistoryCall'

export default class HistoryCall extends Component {
  /*========================Constructor========================*/
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      error: null,
      refreshing: false,
      status: 'initial'
    };
  }
  /*========================Constructor========================*/
  componentDidMount() {
    this.makeRemoteRequest();
  }


  /*========================Get data========================*/
  makeRemoteRequest = () => {
    //getHistoryCall("merlimtest434")
    getRecords()
      .then( (data) => this.setState({
        data: data,
        error: data.error || null,
        loading: false,
        refreshing: false,
        status: 'success'
      }) )
      .catch(error => {
        this.setState({ error, loading: false, status: 'error' });
      });

  };
  /*========================Get data========================*/

  renderList(){
    return (
      <View style={styles.content}>
        <FlatList
          data={this.state.data}
          renderItem={({item}) => (
          <HistoryBox record={item}/>
          )}
          keyExtractor={item => item.history_id}
        />
      </View>
    );
  }
  renderLoadingIndicator(){
    return(
      <View style={styles.indicator}>
        <ActivityIndicator
          animating = {true}
          color = "#495AFF"
          size = "large"
          style = {styles.activityIndicator}
        />
      </View>
    );
  }
  renderError(){
    return(
      <View style={styles.contentImageErr}>
        <Image style={styles.imageError} source={require('../assets/img/historyCall/history_call_error.png')}/>
        <Text style={styles.textHistoryError}>Your calls history could not be retrieved.</Text>
      </View>
    );
  }
  render() {
    const image = '../assets/img/avatar.png'
    const records = this.state.data
    let content = ''
    if(this.state.status == 'initial'){
      content = this.renderLoadingIndicator();
    }else if(this.state.status == 'success'){
      content = this.renderList();
    }else{
      content = this.renderError();
    }
    return(
      <View style={styles.container}>
        <HeaderNav
          title="History Call"
          search={true}
          nav={this.props.navigation}
        />
        {content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  content:{
    padding: 10,
  },
  indicator:{
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  activityIndicator: {
    height: 80
  },
  contentImageErr:{
    display: 'flex',
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },

  textHistoryError:{
    marginTop: 20,
    color: '#BBBBBD'
  }
});
