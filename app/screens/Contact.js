/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  FlatList,
  ActivityIndicator
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNav from '../core/HeaderNav'
import ContactBox from '../core/ContactBox'
import {getContacts} from '../api-client/Contacts'
import {Calls} from '../config/router'
import { getDataUser } from '../core/Functions'

export default class Contact extends Component {
/*========================Contructor========================*/
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      error: null,
      refreshing: false,
      status: 'initial'
    }
  }
/*========================Contructor========================*/

/*========================Excute request========================*/
  componentDidMount() {
    getDataUser()
      .then((data) => {
        return data.split("~")
      })
      .then((data) => {
        this.makeRemoteRequest(data);
      })
  }
/*========================Excute request========================*/

  onCall = (data) => {
    this.props.nav.navigate('UserCall', data)
  }

/*========================Make request to API========================
  @params dataUser, datos del usuario (DeviceId, Username, Thoken)
*/
  makeRemoteRequest = (dataUser) => {
    getContacts(dataUser)
      .then( (data) => this.setState({
        data: data,
        error: data.error || null,
        loading: false,
        refreshing: false,
        status: 'success'
      }) )
      .catch(error => {
        this.setState({ error, loading: false, status: 'error' });
      });
  }
/*========================Make request to API========================*/

/*========================Render contact list========================*/
renderContactList(){
  return (
    <View style={styles.content}>
      <FlatList
        data={this.state.data}
        renderItem={({item}) => (
        <ContactBox data={item} nav={this.props.navigation} />
        )}
        keyExtractor={item => item.userid}
      />
    </View>
  );
}
/*========================Render contact list========================*/

/*========================Render Loading========================*/
renderLoadingIndicator(){
  return(
    <View style={styles.indicator}>
      <ActivityIndicator
        animating = {true}
        color = "#495AFF"
        size = "large"
        style = {styles.activityIndicator}
      />
    </View>
  );
}
/*========================Render Loading========================*/

/*========================Render error========================*/
renderError(){
  return(
    <View style={styles.contentImageErr}>
      <Image style={styles.imageError} source={require('../assets/img/contact/contact_list_error.png')}/>
      <Text style={styles.textContactError}>Your calls history could not be retrieved.</Text>
    </View>
  );
}
/*========================Render error========================*/

  render() {
    let content = '';
    if(this.state.status == 'initial'){
      content = this.renderLoadingIndicator();
    }else if(this.state.status == 'success'){
      content = this.renderContactList();
    }else{
      content = this.renderError();
    }
    return(
      <View style={styles.container}>
        <HeaderNav
          title="Contact"
          search={true}
          nav={this.props.navigation}
        />
        {content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  content:{
    padding: 10
  },
  contactBox:{
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  image:{
    width: 60,
    height: 60
  },
  containerImageName:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  name:{
    color: '#95989A',
    fontSize: 20,
    fontWeight: '400',
    marginLeft: 10
  },
  containerImgCall:{
    flexDirection: 'row',
  },
  imageCall:{
    width: 30,
    height: 30
  },
  indicator:{
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  activityIndicator: {
    height: 80
  },
  contentImageErr:{
    display: 'flex',
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textContactError:{
    marginTop: 20,
    color: '#BBBBBD'
  }
});

//AppRegistry.registerComponent('Networker', () => Networker);
