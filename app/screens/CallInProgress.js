'use strict';

import React, { Component } from 'react';

import {
  AppRegistry,
  Text,
  StyleSheet,
  View,
  Button,
  Image,
  ImageBackground,
  TouchableOpacity,
  ListView,
  TouchableHighlight

} from 'react-native';

import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { getDataUser } from '../core/Functions'
import { Call } from '../api-client/ToCall'

import io from 'socket.io-client';

let container;

import {
  RTCPeerConnection,
  RTCMediaStream,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStreamTrack,
  getUserMedia,
  Platform
} from 'react-native-webrtc';

const configuration = {"iceServers": [{"url": "stun:stun.l.google.com:19302"}]};

const pcPeers = {};
let localStream;

export default class CallInProgress extends Component {

  constructor(props) {

    super(props);

    this.state = {
      callStatus: 'waiting',
      info: 'Initializing',
      status: 'init',
      roomID: '',
      isFront: true,
      selfViewSrc: null,
      remoteList: {},
      textRoomConnected: false,
      textRoomData: [],
      socketId: '',
      textRoomValue: '',
    }

    this.socket    = io.connect('https://react-native-webrtc.herokuapp.com', {transports: ['websocket']});

    this.endpoints = {
      "ROOMS": "http://localhost:3000/rooms/create"
    };

    console.log(this.socket)
    console.log("connected to socket.io");

    this._endCall = this._endCall.bind(this);

    this.initializeSockets();
  }

  initializeSockets(){

    var that = this;

    console.log("Initialized Sockets");

    this.socket.on('connect', function(data) {

          console.log('connect');

          that.getLocalStream(true, function(stream) {

              localStream = stream;

              that.setState({selfViewSrc: stream.toURL()});
              that.setState({status: 'ready', info: 'Please enter or create room ID'});

              //that.createCommunicationWithPeer();

              that.join("habibBorge")

              console.log("Localsream upd server: " + stream.toURL());
          });

    });

    this.socket.on('exchange', function(data){
      that.exchange(data);
    });

    this.socket.on('leave', function(socketId){
      //that.leave(socketId, false);
    });

  }


  createCommunicationWithPeer(roomHash) {

    Call({

      userFrom: "4a4fec8e-a87d-4851-9406-9a6f7c32698a",
      userTo: "77f2b91e-71b0-4baa-adfc-8c382f27d3c6"

    }).then((response) => {
      JSON.parse(response);
    })
    .then((response) => {
      let roomId = JSON.parse(response).roomId;
    })

  }

  exchange(data) {

        var that = this;

        const fromId = data.from;
        let pc;

        if (fromId in pcPeers) {
            pc = pcPeers[fromId];
        } else {
          pc = this.createPC(fromId, false);
        }

        if (data.sdp)
        {
            console.log('Exchange sdp', data);

            pc.setRemoteDescription(new RTCSessionDescription(data.sdp), function () {
                if (pc.remoteDescription.type == "offer")
                  pc.createAnswer(function(desc) {
                      console.log('createAnswer', desc);
                      pc.setLocalDescription(desc, function () {
                        console.log('setLocalDescription', pc.localDescription);
                        that.socket.emit('exchange', {'to': fromId, 'sdp': pc.localDescription });
                      }, that.logError);
                }, that.logError);
          }, this.logError);
      }

      else {
        console.log('exchange candidate', data);
        pc.addIceCandidate(new RTCIceCandidate(data.candidate));
      }
  }


  join(roomID) {

      var that = this;

      this.socket.emit('join', roomID, function(socketIds){

        that.setState({info: 'Joined to rooomessss!'});

        console.log('Join to ROOM ', socketIds);

        for (const i in socketIds) {
          const socketId = socketIds[i];
          that.setState({socketId: socketId});
          that.createPC(socketId, true);
        }

      });
  }

  leave(socketId) {

        console.log('leave', socketId);

        const pc = pcPeers[socketId];
        const viewIndex = pc.viewIndex;

        pc.close();

        delete pcPeers[socketId];

        const remoteList = this.state.remoteList;
        delete remoteList[socketId]

        this.setState({ remoteList: remoteList });
        this.setState({info: 'One peer leave!'});

        this.props.navigation.navigate('Contact')
  }

  getLocalStream(isFront, callback) {

      let videoSourceId;

      getUserMedia({
        audio: true,
        video: false,
      },

      function (stream) {

        console.log('getUserMedia success', stream);
        callback(stream);

      }, this.logError);


  }


  createPC(socketId, isOffer){

      var that = this;

      const pc = new RTCPeerConnection({"iceServers": [{"url": "stun:stun.l.google.com:19302"}]});

      pcPeers[socketId] = pc;

      pc.onicecandidate = function (event) {

          console.log('onicecandidate', event.candidate);

          if (event.candidate) {
            that.socket.emit('exchange', {'to': socketId, 'candidate': event.candidate });
          }
      };

      pc.onnegotiationneeded = function () {

        console.log('onnegotiationneeded');

        if (isOffer) {
          that.createOffer(pc);
        }

      }

      pc.oniceconnectionstatechange = function(event) {
        console.log('oniceconnectionstatechange', event.target.iceConnectionState);

        if (event.target.iceConnectionState === 'completed') {


          setTimeout(() => {
            that.getStats();
          }, 1000);

        }

        if (event.target.iceConnectionState === 'connected') {
          createDataChannel();
        }
      };

      pc.onsignalingstatechange = function(event) {
        console.log('onsignalingstatechange', event.target.signalingState);
      };

      pc.onaddstream = function (event) {

        console.log('onaddstream', event.stream);
        that.setState({info: 'One peer join!'});


        const remoteList = that.state.remoteList;
        remoteList[socketId] = event.stream.toURL();

        that.setState({ remoteList: remoteList });
      };

      pc.onremovestream = function (event) {
        console.log('onremovestream', event.stream);
      };

      pc.addStream(localStream);

      function createDataChannel() {

        console.log("Data channel successs")

        if (pc.textDataChannel) {
          return;
        }

        const dataChannel = pc.createDataChannel("text");

        dataChannel.onerror = function (error) {

          console.log("dataChannel.onerror", error);

        };

        dataChannel.onmessage = function (event) {

          console.log("dataChannel.onmessage:", event.data);
          that.receiveTextData({user: socketId, message: event.data});

        };

        dataChannel.onopen = function () {

          console.log('dataChannel.onopen');
          //container.setState({textRoomConnected: true});
        };

        dataChannel.onclose = function () {

          console.log("dataChannel.onclose");

        };

        pc.textDataChannel = dataChannel;
      }

      return pc;


  }


   getStats() {

     console.log("getStats sss")

     const pc = pcPeers[Object.keys(pcPeers)[0]];

     if (pc.getRemoteStreams()[0] && pc.getRemoteStreams()[0].getAudioTracks()[0])
     {
       const track = pc.getRemoteStreams()[0].getAudioTracks()[0];

       console.log('track', track);

       pc.getStats(track, function(report) {
         console.log('getStats report', report);
       }, this.logError);
    }
  }


  createOffer(pc) {

        var that = this;

        pc.createOffer(function(desc) {

          console.log('createOffer', desc);

          pc.setLocalDescription(desc, function () {
            console.log('setLocalDescription', pc.localDescription);
           // that.socket.emit('exchange', {'to': that.state.socketId, 'sdp': pc.localDescription });
          }, that.logError);

        }, that.logError);
  }

  logError(error) {
      console.log("logError", error);
  }

  componentDidMount() {}

  _endCall(){
    this.leave(this.state.socketId, true);
  }

  makeCall(dataCall){

    Call(dataCall)
      .then((response) => {
        //console.warn(JSON.stringify(response))
      })
      .catch(err => console.error(err));

  }

  mapHash(hash, func) {

    const array = [];

    for (const key in hash) {
      const obj = hash[key];
      array.push(func(obj, key));
    }

    return array;
  }

  render(){

    const { username, name } = this.props.navigation.state.params.data
    console.log(this.props.roomId);
    return (
      <View style={styles.container}>

        <RTCView streamURL={this.state.selfViewSrc} style={styles.selfView}/>

        {
         this.mapHash(this.state.remoteList, function(remote, index) {
           return <RTCView key={index} streamURL={remote} style={styles.remoteView}/>
         })
       }

        <Image style={styles.imageback} source={require('../assets/img/backgroundlogin.png')}>

          <View style={styles.contentAvatar}>
            <Image style={styles.avatar} source={require('../assets/img/avatar.png')}/>
          </View>

          <View style={styles.contentName}>
            <Text style={styles.contact}> {name} </Text>
          </View>

          <Text style={styles.during}> {this.state.status} {this.state.info} </Text>

          <View style={styles.contentSpkAndBtn}>
            <Image style={styles.speaker} source={require('../assets/img/icons/speaker.png')}/>

            <TouchableOpacity  onPress={() => this.props.navigation.navigate('Contact')}>
              <Image style={styles.endcall} source={require('../assets/img/icons/endcall.png')}/>
            </TouchableOpacity>

          </View>
        </Image>
      </View>
    );
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  imageback:{
    flex:1,
    width:null,
    height:null
  },
  contentName:{
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 35
  },
  contact:{
    fontFamily: "Avenir Next",
    fontSize: 38,
    color: "white"
  },
  during:{
    alignSelf: "center",
    color: "white"
  },
  contentAvatar:{
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 110
  },
  avatar:{
    width: 130,
    height: 130,
  },
  contentSpkAndBtn:{
      flexDirection: 'column',
      alignItems: 'center',
      marginTop: 70
  },
  speaker:{

  },
  endcall:{
    marginTop: 50
  }
});
