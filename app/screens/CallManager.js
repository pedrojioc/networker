import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
} from 'react-native';

import { StackNavigator, DrawerNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HeaderNav from '../core/HeaderNav'
import ContactBox from '../core/ContactBox'

import io from 'socket.io-client';

const socket = io.connect('https://react-native-webrtc.herokuapp.com', {transports: ['websocket']});

import {

  RTCPeerConnection,
  RTCMediaStream,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStreamTrack,
  getUserMedia,

} from 'react-native-webrtc';

const configuration = {"iceServers": [{"url": "stun:stun.l.google.com:19302"}]};

const pcPeers = {};
let localStream;

export default class CallManager extends Component {

  render() {
    return (

      <View style={styles.container}>

        <HeaderNav
          title="Contact"
          search={true}
          nav={this.props.navigation}
        />

        <View style={styles.content}>
          <ContactBox />
        </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
});
