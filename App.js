/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  Button
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { StackNavigator, DrawerNavigator } from 'react-navigation'
import Dashboard from './src/screens/Dashboard'
import Contact from './src/screens/Contact'
import HistoryCall from './src/screens/HistoryCall'
import PathOfSuccess from './src/screens/PathOfSuccess'

const DrawerExample = DrawerNavigator(
	{
    First: {
    	screen: Dashboard
    },
    Second: {
    	screen: Contact
    },
    Third: {
    	screen: PathOfSuccess
    },
    Fourth: {
    	screen: HistoryCall
    }
	},
	{
		initialRouteName: 'First'
	}
);

export default DrawerExample;